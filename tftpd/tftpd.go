package main

import "io"
import "os"
import "log"
import "time"
import "github.com/pin/tftp"

// readHandler is called when client starts file download from server
func readHandler(filename string, rf io.ReaderFrom) error {
	file, err := os.Open(filename)
	if err != nil {
		log.Printf("%v\n", err)
		return err
	}
	log.Printf("Reading %s", filename)
	n, err := rf.ReadFrom(file)
	if err != nil {
		log.Printf("%v\n", err)
		return err
	}
	log.Printf("%d bytes sent\n", n)
	return nil
}

// writeHandler is called when client starts file upload to server
func writeHandler(filename string, wt io.WriterTo) error {
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0644)
	if err != nil {
		log.Printf("%v\n", err)
		return err
	}
	log.Printf("Writing %s", filename)
	n, err := wt.WriteTo(file)
	if err != nil {
		log.Printf("%v\n", err)
		return err
	}
	log.Printf("%d bytes received\n", n)
	return nil
}

func main() {
	s := tftp.NewServer(readHandler, writeHandler)
	s.SetTimeout(5 * time.Second) // optional
	err := s.ListenAndServe(":69") // blocks until s.Shutdown() is called
	if err != nil {
		log.Printf("server: %v\n", err)
		os.Exit(1)
	}
}

