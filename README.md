# OpenWrt

  * Super stable!
  * Super modular architecture!
  * Super supportive community!
  * Super awesome product!

Learn from them! Join!

  * https://openwrt.org

Check supported hardware here:

  * https://openwrt.org/toh/start

# How to install

  1. ./clone.sh
  2. ./up.sh
  3. ./make.sh

Flash your [TP-Link Archer A6 (RU) v2](https://openwrt.org/toh/tp-link/archer_c6_v2) router and enjoy. Original firmware can be found here:

  * https://www.tp-link.com/ru/support/download/archer-a6/#Firmware

Known issues:

  * QCA9563 802.11n 3x3 2.4 GHz (has 2 USB but none are connected)
  * tftpd recovery requiring additional switch / router in the middle
  * Maximum 300Mbit/s for Wifi connection (100% sirq CPU load)
  * No U-Boot source, can't rebuild

## How to Flash

1) After issuing last command ./make.sh you gonna get the following file:

`./openwrt-*-squashfs-factory.bin`

2) go to web interface of your router and flash it.

Before flashing install tftpd daemon on your PC and recovery original (or last working) image.

## How to Rollback

You have to use additional switch/router and connect it between your PC and flashing router.

1) download original firmware and rename using `./recovery.sh link` (or take last working `*-squashfs-factory.bin`).

2) Power off router

3) set computer ip using `./recovery.sh ip`

4) Connect lan to your computer

5) run tftpd server `./recovery.sh tftpd`

7) Hold on to the reset button and power on your router (you will see two green led leed up: first and last one)

If you need to debug tftp calls, use `./recovery.sh tcpdump`. tftpd uses two ports. First 69 - is for commands and second are dynamic ports for data transfers.

# Services enabled

  * DHCP (dhcpd)
    * 10.10.5.50-99
  * UPnP (miniupnpd)
  * VPN
    * pptpd (10.10.5.100-119)
    * openvpn (10.10.5.120-139)
    * wireguard (10.10.6.0/24)
  * IPTV (igmproxy)
  * WiFi (http://wiki.openwrt.org/doc/uci/wireless)
  * SSH Server (dropbear)
  * Time Sync (ntpd)
  * Printers Server (p910nd)
    * Apple Bonjour (avahi-daemon)
  * BitTorrent tracker (opentracker)
  * SQM (sqm-scripts)

# Configs

Following config shall be changed before issue a '../up && ./make.sh'. Those steps are nessesery mostly because those
configs holds credentials / passwords or specific settings filled by your ISP.

So please read carrefuly.

## network setup

Original config + my net and IPS setup. More info at:
  * http://wiki.openwrt.org/doc/uci/network

You need to use your 'eth0' mac address.

**_/etc/config/network_**

    config interface 'wan'
        option ifname 'eth0'
        option proto 'static'
        option ipaddr 'xxx.xxx.xxx.xxx'
        option netmask '255.255.255.0'
        option gateway 'xxx.xxx.xxx.xxx'
        option macaddr 'xx:xx:xx:xx:xx:xx'
        option dns '8.8.8.8 8.8.4.4 2001:4860:4860::8888 2001:4860:4860::8844'

## ipv6 teredo

**_/etc/config/miredo_**

    config miredo
        option enabled 1

**_/etc/config/firewall_**

    config defaults
        option masq6 '1'

## ipv6 tunnelbroker.net

**_/etc/config/network_**

    config interface wan6
        option proto 6in4
        option peeraddr '216.66.80.90'
        option ip6addr '2001:470:xx:xxx::2/64'
        option ip6prefix '2001:470:xx:xxx::/64'
        option tunnelid '222333'
        option username 'he_name'
        option password 'md5_password'

    config route 'ipv6'
        option interface 'wan'
        option target '216.66.80.90'
        option netmask '255.255.255.255'
        option gateway 'xxx.xxx.xxx.xxx'
        option metric 0
        
**_/etc/config/firewall_**

    config defaults
        option masq6 '0'

## ipv6 wireguard provider

    config interface 'wan6r48'
      option disabled 0
      option proto wireguard
      option private_key 'wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww='
      list addresses '2a00:1003:907b::2/48'
      option ip6prefix '2a00:1003:907b:1::2/64'

    config wireguard_wan6r48
      option disabled 0
      option public_key 'WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW='
      list allowed_ips '::/1'
      list allowed_ips '8000::/1'
      option endpoint_host 'node.profider.org'
      option endpoint_port '51896'
      option persistent_keepalive '60'
      option route_allowed_ips '1'

## wifi setup

Pretty original config + my ssid name and password. Don't forget to change macaddr option, which is nessesery
to lookup interface. Or just skip it, after login run 'wifi' will configure your wifi automatically. You have to
use 'eth0 or wlan0' interface mac address for proper interface lookup.

  * https://openwrt.org/docs/guide-user/network/wifi/basic

**_/etc/config/wireless_**

    config wifi-device  radio0
        option macaddr  xx:xx:xx:xx:xx:xx
  
    config wifi-iface
        option device   radio0
        option network  lan
        option mode     ap
        option ssid     wifi.local
        option encryption 'psk2'
        option key      '12345678'

## VPN service

You can use booth openvpn and pptp vpns of you choose.

### openvpn server

You have to prepare followings files in files/etc/openvpn/

    openssl dhparam -out dh1024.pem 1024

    openssl genrsa -out ca.key 4096
    openssl req -new -x509 -days 3650 -key ca.key -out ca.crt

    openssl genrsa -out server.key 4096
    openssl req -new -key server.key -out server.csr
    openssl x509 -req -days 3650 -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt

    openssl genrsa -out client.key 4096
    openssl req -new -key client.key -out client.csr
    openssl x509 -req -days 3650 -in client.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out client.crt

**_client.conf_**

    client
    dev tap

    proto udp
    remote openwrt.lan 1194
    resolv-retry infinite

    nobind

    persist-key
    persist-tun


    <ca>
      [ ca.crt file content here ]
    </ca>
    <cert>
      [ client.crt file content here ]
    </cert>
    <key>
      [ client.key file content here ]
    </key>
    comp-lzo

    script-security 2

### openvpn client

For vpn or ipv6 purposes:

**_/etc/config/firewall_**

    config zone                    
            option name 'wan'      
            option network 'wan wan6'
            ...
            option masq6 '1' 

**_/etc/config/network_**

    config interface wan6
            option device 'tun0'
            option proto static
            option ip6addr 'fd77:550d:5fb8::1/48'
            option ip6prefix 'fd77:550d:5fb8::/48'
        
**_/etc/config/openvpn_**

    config 'openvpn' 'client'
            option enable 1
            option client 1
            option config /etc/openvpn/6project.ovpn

**_/etc/openvpn/6project.ovpn_**

    client
    dev tun0
    ...

### pptpd

Set your pptp range, and your users name and pasword:

**_files/etc/config/pptpd_**

    config service 'pptpd'
            option 'enabled' '0'
            option 'localip' '10.10.5.1'
            option 'remoteip' '10.10.5.20-30'
                            
    config 'login'
            option 'username' 'user1'
            option 'password' 'password1'
        
## IPTV

Pretty original config. You have to check `logread` to see your ISP altnet mask. And allow firewall to pass
igmp trafic.

  * http://wiki.openwrt.org/doc/howto/udp_multicast

**_/etc/config/igmpproxy_**

    config phyint
        option network wan
        option direction upstream
        list altnet 77.94.170.0/24

## ntpd

Please fill up your timezone in the following config.

  * http://wiki.openwrt.org/doc/uci/system

**_/etc/config/system_**

    config system
        option hostname 'OpenWrt'
        option timezone 'MSK-4'

## ssh easy acess

Don't forget to make your live easier. Put your id-rsa.put into authorized_keys on the router.

**<i>/etc/dropbear/authorized_keys</i>**

    [your key]
    
## p910nd

Setting up printer is quite pain. First of all, you can't install cups stack since you gonna need more then 100MB of fonts and drivers to run it smoothly. Most routers has 4 - 8 MB flash images.

That is why, you have to passthrou your usb printer as network port printer.

  * http://wiki.openwrt.org/doc/howto/p910nd.server

To setup HP LaserJet 1020 you need one script and one firmware file. Details below:

  * [./files/etc/hotplug.d/usb/20-hp1020](./files/etc/hotplug.d/usb/20-hp1020)

## samba

Connect USB external harddrive formatted as ext4 or vfat :) to your router and fix block mount script. I recommend my external harddrive structure. It should have two folders _www_ and _local_. Booth have mutual access rw and ro. Readonly access opened by default, to change folder contentn you have to connect to the _www-rw_ hidden share or _local-rw_ share.

**_/etc/config/fstab_**

	config 'global'
	        option  anon_swap       '0'
	        option  anon_mount      '0'
	        option  auto_swap       '1'
	        option  auto_mount      '1'
	        option  delay_root      '5'
	        option  check_fs        '1'
	
	config 'mount'
	        option  target  '/mnt/shared'
	        option  uuid    '11111111-1111-1111-1111-111111111111'
	        option  enabled '1'
	        option 'options'  'rw,sync'
	        option 'enabled_fsck' '1'

**_/etc/config/samba_**

	config samba
	        option 'name'                   'OpenWrt'
	        option 'workgroup'              'WORKGROUP'
	        option 'description'            'OpenWrt'
	        option 'homes'                  '0'
	
	config 'sambashare'
	        option 'name' 'www'
	        option 'path' '/mnt/shared/www'
	        option 'guest_ok' 'yes'
	        option 'read_only' 'yes'
	
	config 'sambashare'
	        option 'name' 'www-rw'
	        option 'path' '/mnt/shared/www'
	        option 'guest_ok' 'yes'
	        option 'create_mask' '0700'
	        option 'dir_mask' '0700'
	        option 'browseable' 'no'
	
	config 'sambashare'
	        option 'name' 'local'
	        option 'path' '/mnt/shared/local'
	        option 'guest_ok' 'yes'
	        option 'read_only' 'yes'
	
	config 'sambashare'
	        option 'name' 'local-rw'
	        option 'path' '/mnt/shared/local'
	        option 'guest_ok' 'yes'
	        option 'create_mask' '0700'
	        option 'dir_mask' '0700'
	        option 'browseable' 'no'

## SQM

SQM (Smart Queue Management) helps keep connection alive when ISP failed to work properly at maximum internet speed (mostly happens with ADSL connections when reaching upload upload speed which prevents data to be transfered, but also happens with wired/ethernet connections). When you take full bandwidth internet while downloading torrent at maxmium speed or using game manager to download games (battle.net / gog) internet may stop working properly showing high ping, losing packets or browser stop opening pages and showing busy cirlce instead of page. Then you need to enable SQM on your router and set maximum download and upload speeds of your ISP connection. Usually you need to reduce speed to 80% from your ISP / speedtest value.

Lets say you have 70 MB/s, then you have to limit your connection to 60000 kbp/s.

If your provider has dynamic bandtiwth for day and night use crontab to set limits dynamically. You can find crontab in example sqm config file.
