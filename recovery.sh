#!/bin/bash
#
# /etc/network/interfaces.d/eth0:0
#
# auto eth0:0
# allow-hotplug eth0:0
# iface eth0:0 inet static
#    address 192.168.0.66
#    netmask 255.255.255.0
#
# apt install tftpd-hpa
#
# chmod o+rw /srv/tftp
# 

: ${EN:=$(ip -4 route ls | awk '/^default/{print $5}')}
: ${HOST:=rpi}

case $1 in
firmware) # https://www.tp-link.com/us/support/download/archer-a6/v2/#Firmware
  curl -LOJ 'https://static.tp-link.com/upload/firmware/2023/202311/20231103/Archer%20A6(US)_V2_230830.zip'
;;
ip)
  sudo ip addr add 192.168.0.66/24 dev $EN
;;
tcpdump)
  sudo tcpdump -n -i $EN host 192.168.0.86
;;
tftpd)
  [ -e ./tftpd/tftpd ] || (cd tftpd && go get && go build)
  sudo ./tftpd/tftpd
;;
sync)
  rsync -avP *.bin $HOST:/srv/tftp/
;;
esac
