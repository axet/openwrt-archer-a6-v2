#!/bin/bash
#
# -c           attempt to preserve all changed files in /etc/
# -n           do not save configuration over reflash
#
# ./sysupgrade.sh -n; sleep 55; beep; sleep 60; beep

set -e

HOST=$(awk '/hostname/ {gsub(/'"'"'/, "", $3); print $3}' files/etc/config/system)
: ${BIN:="*-squashfs-sysupgrade.bin"}
: ${LAN:="root@$HOST.lan"}

# tar -c $BIN | ssh -C "$LAN" "tar -xv -C /tmp && sh --login -i /sbin/sysupgrade -v $@ /tmp/$BIN"

cat $BIN | ssh -C "$LAN" "/sbin/mtd -r write - firmware"
