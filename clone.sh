#!/bin/bash

set -e

which apt-get && sudo apt-get install -y libncurses5-dev gawk subversion zlib1g-dev libssl-dev libxml-parser-perl rsync

git clone --depth 1 -b v23.05.4 https://git.openwrt.org/openwrt/openwrt.git trunk
