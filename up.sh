#!/bin/bash

set -e

REV="$1"

pullrebase() {
  if [[ $(git tag -l $(git symbolic-ref -q --short HEAD | sed -e 's/^heads\///')) ]]; then
    echo "not updating tag branch"
    return
  fi
  N=$(git status -s --untracked-files=no | wc -l)
  if [ $N -eq 0 ]; then
    git pull --rebase
  else
    git stash
    git pull --rebase
    git stash pop
  fi
}

if [ -z "$REV" ]; then
  (cd trunk && pullrebase)
  (cd trunk && ./scripts/feeds update -a)
else
  (cd trunk && git fetch && git checkout -b "$REV")
  (cd trunk && ./scripts/feeds update -a)
fi

./install.sh

(cd trunk && make download)
