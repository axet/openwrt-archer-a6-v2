#!/bin/bash

set -e

backup() {
  local FILE="$1"
  local DATE=$(date -r $FILE +%F)
  mv "$FILE" "$FILE.$DATE"
}

if [[ "$OSTYPE" == "darwin"* ]]; then
  CPU=$(sysctl -n hw.ncpu)
else
  CPU=$(nproc)
fi

( cd trunk && make -j$CPU "$@" )

wait

for F in *.bin; do
  [ -f "$F" ] || continue
  [ ! -L "$F" ] || continue
  backup "$F"
done

mv -v ./trunk/bin/*/*/*/*-squashfs-* .
