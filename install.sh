#!/bin/bash

set -e

install_key() {
  KEY="$1"
  TARGET="$2"

  VAL=$(cat "$KEY")
  grep "$VAL" "$TARGET" || echo "$VAL" >> "$TARGET"
}

# revert all manual changes
(cd trunk && git reset --hard)

# clean packages data
(cd trunk/package && git clean -dxf .)

# apply patches to the trunk
(cd trunk && for P in $(ls -A ../patches/*.patch 2> /dev/null); do patch -R -p1 --dry-run --silent -N <$P 1>/dev/null || patch -p1 < $P; done)
# add new files to package/ (contains patches && package files)
(mkdir -p packages && rsync -ar packages/ trunk/package/)
(mkdir -p tools && rsync -ar tools/ trunk/tools/)

IFS_old=$IFS
IFS=$'\n'
PORTS=($(cat config-packages))
IFS=$IFS_old

# enable modules to install
for (( i=0; i<${#PORTS[@]}; i++ )); do
  PORT=(${PORTS[$i]})
  ( [ "${#PORT[@]}" -eq 0 ] || [[ ${PORT:0:1} == '#' ]] ) && echo "${PORT[@]}" && continue
  (cd trunk && ./scripts/feeds install -d n "${PORT[@]}")
done

# install configs
(cd trunk && cat ../config-kernel >> target/linux/ath79/config-*)
(cd trunk && cp ../config-trunk .config)
(cd trunk && make defconfig)

# update custom files
(rsync -ar --delete files/ trunk/files/)

# enable current machine eddsa automatic login
install_key $HOME/.ssh/id_ed25519.pub trunk/files/etc/dropbear/authorized_keys
chmod -R u=rwX,og= trunk/files/etc/dropbear/

# fix passwd files perms
[ -e trunk/files/etc/passwd ] && chmod 644 trunk/files/etc/passwd
[ -e trunk/files/etc/shadow ] && chmod 600 trunk/files/etc/shadow
